use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Person {
    pub id: u32,
    pub username: String,
    pub first_name: String,
    pub last_name: String,
    pub division: Option<u32>,
    pub domain: Option<u32>,
    #[serde(flatten)]
    other: serde_json::Value,
}
